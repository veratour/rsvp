# RSVP (Répondez S'il Vous Plaît) 0.1

A simple invitation platform based on [Kohana 3](http://kohanaframework.org/) and [Bootstrap](http://twitter.github.com/bootstrap/).

The platform is in its early stage of development.