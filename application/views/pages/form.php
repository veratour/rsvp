<div class="row">
    <div class="span12"> <!-- TODO: c'è una sidebar??? -->
        <?php print View::factory('partials/title', array('title' => $title, 'page_title' => $page_title, 'page_subtitle' => $page_subtitle)); ?>
        <?php print View::factory('partials/messages', array('messages' => $messages)); ?>
        <?php print View::factory('partials/actionbar', array('actions' => $actions)); ?>
        <div class="well well-large">
                <?php print View::factory('contents/'.$content_template, compact(array_keys(get_defined_vars()))); ?>
        </div>
    </div>
</div>