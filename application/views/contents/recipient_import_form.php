<h3>Importa destinatari <small>carica un file .CSV</small></h3>
<p>Per creare un file .csv corretto, assicurati di avere in excel un file con i nomi delle colonne nella prima riga. Una delle colonne <strong>deve necessariamente</strong> contenere l'indirizzo email.<br />
    Esporta il file cliccando sul menù <em>File</em> → <em>Salva con nome</em> e scegli dal menù <em>Formato</em> "Valori separati da virgola".</p>
<form class="form-horizontal" method="post" action="<?php print URL::site('recipientgroup/import_csv/'.$grp_id); ?>" enctype="multipart/form-data">
<fieldset>
    <div class="control-group">
        <label class="control-label" for="file"><?php print __('File'); ?></label>
        <div class="controls"><input type="file" name="file" id="file" class="input-xlarge" /></div>
    </div>
     <div class="form-actions">
        <button type="submit" class="btn btn-large btn-primary"><i class="icon-ok icon-white"></i> <?php print __('Carica'); ?></button>
        <a href="<?php print URL::site('recipientgroup/recipients/'.$grp_id); ?>" class="btn btn-large"><i class="icon-remove"></i> <?php print __('Annulla'); ?></a>
    </div>
</fieldset>
</form>

<h3>Aggiungi destinatari singoli</h3>
<form class="form-horizontal" method="post"<?php if(isset($form_action)) print ' action="'.$form_action.'"'; ?>>

TODO
</form>