<form class="form-horizontal" method="post"<?php if(isset($form_action)) print ' action="'.$form_action.'"'; ?>>
    <input type="hidden" name="id" value="<?php print $form->id; ?>" />
    
<fieldset>
    <legend>Nome</legend>
    <div class="control-group">
        <label class="control-label" for="name"><?php print __('Nome'); ?></label>
        <div class="controls"><input type="text" name="name" id="name" class="input-xlarge" value="<?php print $form->name; ?>" /></div>
    </div>
    <div class="control-group">
        <label class="control-label">Campi</label>
        <div class="controls">
            
        <div class="mini-actionbar btn-group"">
            <button class="btn btn-primary btn-mini" type="button" data-toggle="modal" data-target="#form-addfield"><i class="icon-plus icon-white"></i> <?php print __('Aggiungi campo'); ?></button>
        </div>
            
        <table class="table table-striped table-bordered table-condensed">
            <thead>
                <tr>
                    <th><?php print __('Nome'); ?></th>
                    <th><?php print __('Tipo'); ?></th>
                    <th><?php print __('Default'); ?></th>
                    <th width="20%"><?php print __('Azioni'); ?></th>
                </tr>
            </thead>
            <tbody>
            <?php foreach($form->fields->find_all() as $f): ?>
                <tr>
                    <td><input type="hidden" name="formfield_id[]" value="<?php print $f->id; ?>" /><?php print $f->label; ?></td>
                    <td><?php print $f->type; ?></td>
                    <td><?php print $f->default; ?></td>
                    <td><?php
        
                    print HTML::btn_modify(URL::site('form/field_edit/'.$f->id)).' ';
                    print HTML::btn_delete(URL::site('form/field_delete/'.$f->id));

                    ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
       </div>
    </div>
    <div class="form-actions">
        <button type="submit" class="btn btn-large btn-primary"><i class="icon-ok icon-white"></i> <?php print __('Salva'); ?></button>
        <a href="<?php print URL::site('form'); ?>" class="btn btn-large"><i class="icon-remove"></i> <?php print __('Annulla'); ?></a>
    </div>
</fieldset>
</form>

<div class="modal hide fade" id="form-addfield">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3><?php print __('Aggiungi campo'); ?></h3>
  </div>
  <div class="modal-body">
    <p>One fine body…</p>
  </div>
  <div class="modal-footer">
    <a href="#" class="btn" data-dismiss="modal" aria-hidden="true"><?php print __('Chiudi'); ?></a>
    <a href="#" class="btn btn-primary">Save changes</a>
  </div>
</div>