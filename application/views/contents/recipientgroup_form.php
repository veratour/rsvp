<form class="form-horizontal" method="post"<?php if(isset($form_action)) print ' action="'.$form_action.'"'; ?>>
<fieldset>
    <legend>Gruppo Destinatari</legend>
    <div class="control-group">
        <label class="control-label" for="tour_name"><?php print __('Nome'); ?></label>
        <div class="controls"><input type="text" name="name" id="tour_name" class="input-xlarge" value="<?php if (isset($form_values['name'])) print $form_values['name']; ?>" /></div>
    </div>
    <div class="form-actions">
        <button type="submit" class="btn btn-large btn-primary"><i class="icon-ok icon-white"></i> <?php print __('Salva'); ?></button>
        <a href="<?php print URL::site('recipientgroup'); ?>" class="btn btn-large"><i class="icon-remove"></i> <?php print __('Annulla'); ?></a>
    </div>
</fieldset>
</form>