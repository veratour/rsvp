<?php

class Model_Landingpage extends ORM {
    
    protected $_belongs_to = array(
        'events' => array(),
        'messages' => array()
    );
    
    public function rules() {
        return array(
            'title' => array(
                array('not_empty')
            ),
            'html' => array(
                array('not_empty')
            )
        );
    }
}