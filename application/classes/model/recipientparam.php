<?php

class Model_Recipientparam extends ORM {
    
    protected $_belongs_to = array(
        'recipients' => array()
    );
    
    
    public function rules() {
        return array(
            'key' => array(
                array('not_empty')
            ),
            'type' => array(
                array('not_empty')
            ),
            'value' => array(
                array('not_empty')
            ),
        );
    }
}