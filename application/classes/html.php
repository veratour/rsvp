<?php

class HTML extends Kohana_HTML {
    
    public static function iconize_yes_no($val, $white = false) {
        $ico = (bool) $val ? 'icon-ok' : 'icon-remove';
        $white = (bool) $white ? ' icon-white' : '';
        return '<i class="'.$ico.$white.'"></i>';
    }
    
    public static function btn_modify($url, $label = 'Modifica', $disabled = false) {
        $class = $disabled ? ' disabled' : '';
        $url   = $disabled ? '#' : $url;
        return '<a href='.$url.' class="btn btn-mini'.$class.'"><i class="icon-pencil"></i> '. __($label).'</a>';
    }
    
    public static function btn_delete($url, $label = 'Cancella', $disabled = false) {
        $class = $disabled ? ' disabled' : '';
        $url   = $disabled ? '#' : $url;
        return '<a href='.$url.' class="btn btn-mini btn-danger'.$class.'" onclick="return confirm(\''. addslashes(__('Sei sicuro di voler cancellare l\'elemento? L\'operazione non è annullabile.')) .'\');"><i class="icon-trash icon-white"></i> '. __($label).'</a>';
    }
    
    public static function btn_list($url, $label = 'Vedi', $disabled = false) {
        $class = $disabled ? ' disabled' : '';
        $url   = $disabled ? '#' : $url;
        return '<a href='.$url.' class="btn btn-mini btn-info'.$class.'"><i class="icon-th-list icon-white"></i> '. __($label).'</a>';
    }
    
    public static function btn_add($url, $label = 'Aggiungi', $disabled = false) {
        $class = $disabled ? ' disabled' : '';
        $url   = $disabled ? '#' : $url;
        return '<a href='.$url.' class="btn btn-mini btn-primary'.$class.'"><i class="icon-plus icon-white"></i> '. __($label).'</a>';
    }
    
}