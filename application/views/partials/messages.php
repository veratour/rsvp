<?php if (isset($messages['error']) && count($messages['error']) > 0): ?>
<div class="alert alert-error">
    <button class="close" data-dismiss="alert">×</button>
<?php foreach($messages['error'] as $error): ?>
    <p><?php print $error; ?></p>
<?php endforeach; ?>
</div>
<?php endif; ?>


<?php if (isset($messages['success']) && count($messages['success']) > 0): ?>
<div class="alert alert-success">
    <button class="close" data-dismiss="alert">×</button>
<?php foreach($messages['success'] as $success): ?>
    <p><?php print $success; ?></p>
<?php endforeach; ?>
</div>
<?php endif; ?>


<?php if (isset($messages['info']) && count($messages['info']) > 0): ?>
<div class="alert alert-info">
    <button class="close" data-dismiss="alert">×</button>
<?php foreach($messages['info'] as $info): ?>
    <p><?php print $info; ?></p>
<?php endforeach; ?>
</div>
<?php endif; ?>
