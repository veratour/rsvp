$().ready( function() {

    // common ajax
    $('body').ajaxStart( function() {
        $(this).addClass('wait');
    });
    $('body').ajaxStop( function() {
        $(this).removeClass('wait');
    });
    
    // duplicator
    var duplicator1_re = /(.*)_([0-9]+)_(.*)/;
    var duplicator2_re = /(.*)\[([0-9]+)\](.*)/;
    $('.duplicator').click( function(e) {
        var n = $('.duplicate').length;
        $(this).attr('data-newrow', n+1);
        e.preventDefault();
        var el = $('.duplicate:last-child');
        var cl = el.clone(true);
        $('input, label, textarea', cl).each( function() {
            var attr_for = $(this).attr('for');
            var attr_id  = $(this).attr('id');
            var attr_name  = $(this).attr('name');
            if(attr_for != undefined) {
                $(this).attr('for', attr_for.replace(duplicator1_re, '$1_'+ n +'_$3'));
            }
            if (attr_id != undefined) {
                $(this).attr('id', attr_id.replace(duplicator1_re, '$1_'+ n +'_$3'));
            }
            if (attr_name != undefined) {
                $(this).attr('name', attr_name.replace(duplicator2_re, '$1['+ n +']$3'));
            }
            if ($(this).val()) $(this).val('');
            if ($(this).attr('checked')) $(this).removeAttr('checked');
        });
        $('a.close', cl).removeAttr('data-remove');
        cl.insertAfter(el);
        $('[data-datepicker]').datepicker();
    });
    
    
    $('.duplicate .close').click( function() {
        var id = $(this).attr('data-remove');
        var url = $(this).attr('data-url');
        if (id != undefined) {
            $.post(base_url+url, {id: id});
        }
        if ($('.duplicate').length == 1) {
            $(this).parent().find('input, textarea').each( function() {
                if ($(this).val()) $(this).val('');
                if ($(this).attr('checked')) $(this).removeAttr('checked');
            });
            $(this).removeAttr('data-remove');
        } else {
            $(this).parent().hide(
                function() {$(this).remove();}
            );
        }
    });
    
    // toggler (per la pagina dell'importazione CSV dei recipients
    var toggler_states = ['ok', 'remove', 'envelope'];
    
    $('.import-toggler').click( function() {
        var actual_state = $(this).attr('data-state');
        var actual_state_id = $.inArray(actual_state, toggler_states);
        if (actual_state_id == toggler_states.length - 1) actual_state_id = -1;
        var new_state_id = ++actual_state_id;
        var new_state = toggler_states[new_state_id];
        $(this).attr('data-state', new_state);
        $('i', this).removeClass('icon-'+actual_state).addClass('icon-'+new_state);
        $(this).parent().removeClass('state-'+actual_state).addClass('state-'+new_state);
    });
    
    $('#import-csv-form').submit( function() {
        var i = 0;
        $('#import-csv-table thead th').each( function() {
            var v = $('button', this).attr('data-state');
            $('#import-csv-form').append('<input type="hidden" name="csv-field['+ (i++) +']" value="'+v+'" />');
        });
    });
    
    if ($('.public-login .alert-error').length > 0) {
        var offset = 15;
        var el = $('.public-login .well');
        for (i = 0; i < 6; i++) {
            el.animate({
                    marginLeft:  ((i%2==0) ? '-' : '') + offset + 'px',
                    marginRight: ((i%2==0) ? '' : '-') + offset + 'px'
                }, 70
            );
        }
        
        el.animate({
                marginRight: 0,
                marginLeft: 0
            }, 35
        );
    }
    
    
});