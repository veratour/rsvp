<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Form extends Controller_Private {

    function action_index() {        
        $this->template->title = __('Form');
        $this->template->page_template = 'table';
        $this->template->content_template = 'form_table';
        
        $this->template->actions = array(
            array('url' => URL::site('form/add'), 'class' => 'btn-primary', 'icon_class' => 'icon-plus icon-white', 'label' => 'Aggiungi nuovo')
        );
        
        $forms = ORM::factory('form')->filter_by_active_user();
        
        $pagination = Pagination::factory(array(
                'total_items' => $forms->count_all()
            )
        );
        
        $forms = $forms->offset($pagination->offset)->limit($pagination->items_per_page)->find_all();
        
        $this->template->pagination = $pagination;
        $this->template->forms = $forms;
    }
    
    
    function action_edit() {
        $id = $this->request->param('id');
        
        $form = ORM::factory('form')->find($id);
        
        $this->template->title = __('Modifica form');
        $this->template->page_subtitle = $form->name;
        $this->template->page_template = 'form';
        $this->template->content_template = 'form_form';
        $this->template->form = $form;
    }
    
    
    function action_field_edit() {
        $fid = $this->request->param('id');
    }
    
    function action_field_delete() {
        $fid = $this->request->param('id');
    }
    
}