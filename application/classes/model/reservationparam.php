<?php

class Model_Reservationparam extends ORM {
    
    protected $_belongs_to = array(
        'reservations' => array()
    );
    
    
    public function rules() {
        return array(
            'key' => array(
                array('not_empty')
            ),
            'value' => array(
                array('not_empty')
            ),
        );
    }
}