<form class="form-horizontal" method="post"<?php if(isset($form_action)) print ' action="'.$form_action.'"'; ?>>
<fieldset>
    <div class="control-group">
        <label class="control-label" for="username">Username</label>
        <div class="controls">
            <input type="text" class="input-xlarge" id="username" name="username">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="password">Password</label>
        <div class="controls">
            <input type="password" class="input-xlarge" id="password" name="password">
        </div>
    </div>
    <div class="form-actions">
        <button type="submit" class="btn btn-large btn-primary">Login</button>
    </div>
</fieldset>
</form>