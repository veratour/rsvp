<?php

return array(
    ':field must not be empty' => 'Il campo ":field" è obbligatorio',
    ':field must be a valid time' => 'Il campo ":field" deve essere un orario valido',
    ':field must be a date' => 'Il campo ":field" deve essere una data valida',
    'name' => 'nome',
    'location' => 'luogo',
    'date_from' => 'dal (data)',
    'date_to' => 'al (data)',
    'time_from' => 'dal (ora)',
    'time_to' => 'al (ora)',
    ':field must contain a valid file' => 'Il file che si sta cercando di caricare non è valido',
    'The uploaded file must have the correct filetype' => 'Il formato del file che si sta cercando di caricare non è supportato'
);