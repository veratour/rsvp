<?php

class Flashmessage {
    
    static $messages;
    static $session_key = 'messages';
    
    public static function set($type, $text) {
        Flashmessage::$messages[$type][] = $text;
        Session::instance()->set(Flashmessage::$session_key, Flashmessage::$messages);
    }
    
    public static function get() {
        $m = Session::instance()->get(Flashmessage::$session_key);
        Session::instance()->delete(Flashmessage::$session_key);
        return $m;
    }
}