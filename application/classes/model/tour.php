<?php

class Model_Tour extends ORM {
    
    protected $_belongs_to = array(
        'users' => array()
    );
    
    protected $_has_many = array(
        'events' => array()
    );
    
    
    public function rules() {
        return array(
            'name' => array(
                array('not_empty')
            )
        );
    }
    
    public function filter_by_active_user() {
        return $this->where('user_id', '=', Auth::instance()->get_user()->id);
    }
    
    public function add_events_details() {
        $subquery_min = DB::select('tour_id', array('MIN("date_from")','date_from'))
                        ->from('events')
                        ->group_by('tour_id');
        $subquery_max = DB::select('tour_id', array('MAX("date_to")','date_to'))
                        ->from('events')
                        ->group_by('tour_id');
        $subquery_count = DB::select('tour_id', array('COUNT("id")','events_count'))
                        ->from('events')
                        ->group_by('tour_id');
        
        return $this->select('*', 'a.*', 'b.*', 'c.*')
                    ->join(array($subquery_min,   'a'), 'LEFT')->on('tour.id','=','a.tour_id')
                    ->join(array($subquery_max,   'b'), 'LEFT')->on('tour.id','=','b.tour_id')
                    ->join(array($subquery_count, 'c'), 'LEFT')->on('tour.id','=','c.tour_id');
    }
}