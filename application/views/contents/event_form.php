<form class="form-horizontal" method="post"<?php if(isset($form_action)) print ' action="'.$form_action.'"'; ?>>
<fieldset>
    <legend>Evento</legend>
    <div class="control-group">
        <label class="control-label" for="tour_name"><?php print __('Nome'); ?></label>
        <div class="controls"><input type="text" name="tour[name]" id="tour_name" class="input-xlarge" value="<?php if (isset($form_values['tour']['name'])) print $form_values['tour']['name']; ?>" /></div>
    </div>
    <div class="control-group">
        <label class="control-label" for="tour_is_open"><?php print __('Evento aperto'); ?></label>
        <div class="controls"><input type="checkbox" name="tour[is_open]" value="1" id="tour_is_open" <?php if (isset($form_values['tour']['is_open']) && $form_values['tour']['is_open']) print 'checked="checked"'; ?> /></div>
    </div>
</fieldset>

<fieldset>
    <legend>Appuntamenti</legend>
    <div id="duplicate-list">
    <?php
    
    if (isset($form_values['event'])) {
        $ec = count($form_values['event']);
    } else {
        $ec = 1;
    }
    
    for ($i = 0; $i < $ec; $i++):
    ?>
    <div class="duplicate">
        <a href="javascript:;" class="close" data-url="tour/delete_event" <?php if (isset($form_values['event'][$i]['id'])) print 'data-remove="'.$form_values['event'][$i]['id'].'"'; ?>>×</a>
        <input type="hidden" name="event[<?php print $i; ?>][id]" id="event_<?php print $i; ?>_id" value="<?php if (isset($form_values['event'][$i]['id'])) print $form_values['event'][$i]['id']; ?>" />
        <div class="control-group">
            <label class="control-label" for="event_<?php print $i; ?>_name"><?php print __('Nome'); ?></label>
            <div class="controls"><input type="text" name="event[<?php print $i; ?>][name]" id="event_<?php print $i; ?>_name" class="input-xlarge" value="<?php if (isset($form_values['event'][$i]['name'])) print $form_values['event'][$i]['name']; ?>" /></div>
        </div>

        <div class="control-group">
            <label class="control-label" for="event_<?php print $i; ?>_date_from"><?php print __('Dal'); ?></label>
            <div class="controls">
                <input type="text" name="event[<?php print $i; ?>][date_from]" id="event_<?php print $i; ?>_date_from" data-datepicker="datepicker" class="input-small" value="<?php if (isset($form_values['event'][$i]['date_from'])) print $form_values['event'][$i]['date_from']; ?>" placeholder="<?php print __('Data (aaaa-mm-gg)'); ?>"/>
                <input type="text" name="event[<?php print $i; ?>][time_from]" id="event_<?php print $i; ?>_time_from" class="input-small" value="<?php if (isset($form_values['event'][$i]['time_from'])) print $form_values['event'][$i]['time_from']; ?>" placeholder="<?php print __('Ora (oo:mm)'); ?>" />
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="event_<?php print $i; ?>_date_to"><?php print __('Al'); ?></label>
            <div class="controls">
                <input type="text" name="event[<?php print $i; ?>][date_to]" id="event_<?php print $i; ?>_date_to" data-datepicker="datepicker" class="input-small" value="<?php if (isset($form_values['event'][$i]['date_to'])) print $form_values['event'][$i]['date_to']; ?>" placeholder="<?php print __('Data (aaaa-mm-gg)'); ?>" />
                <input type="text" name="event[<?php print $i; ?>][time_to]" id="event_<?php print $i; ?>_time_to" class="input-small" value="<?php if (isset($form_values['event'][$i]['time_to'])) print $form_values['event'][$i]['time_to']; ?>" placeholder="<?php print __('Ora (oo:mm)'); ?>" />
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="event_<?php print $i; ?>_location"><?php print __('Luogo'); ?></label>
            <div class="controls"><input type="text" name="event[<?php print $i; ?>][location]" id="event_<?php print $i; ?>_location" class="input-xlarge" value="<?php if (isset($form_values['event'][$i]['location'])) print $form_values['event'][$i]['location']; ?>" /></div>
        </div>

        <div class="control-group">
            <label class="control-label" for="event_<?php print $i; ?>_description"><?php print __('Descrizione'); ?></label>
            <div class="controls"><textarea name="event[<?php print $i; ?>][description]" id="event_<?php print $i; ?>_description" class="input-xlarge"><?php if (isset($form_values['event'][$i]['description'])) print $form_values['event'][$i]['description']; ?></textarea></div>
        </div>

        <div class="control-group">
            <label class="control-label" for="event_<?php print $i; ?>_city"><?php print __('Città'); ?></label>
            <div class="controls"><input type="text" name="event[<?php print $i; ?>][city]" id="event_<?php print $i; ?>_city" class="input-xlarge" value="<?php if (isset($form_values['event'][$i]['city'])) print $form_values['event'][$i]['city']; ?>" /></div>
        </div>

        <div class="control-group">
            <label class="control-label" for="event_<?php print $i; ?>_address"><?php print __('Indirizzo'); ?></label>
            <div class="controls"><input type="text" name="event[<?php print $i; ?>][address]" id="event_<?php print $i; ?>_address" class="input-xlarge" value="<?php if (isset($form_values['event'][$i]['address'])) print $form_values['event'][$i]['address']; ?>" /></div>
        </div>

        <div class="control-group">
            <label class="control-label" for="event_<?php print $i; ?>_country"><?php print __('Nazione'); ?></label>
            <div class="controls"><input type="text" name="event[<?php print $i; ?>][country]" id="event_<?php print $i; ?>_country" class="input-xlarge" value="<?php if (isset($form_values['event'][$i]['country'])) print $form_values['event'][$i]['country']; ?>" /></div>
        </div>
    </div>
    <?php endfor; ?>
    </div>
        <div class="control-group">
            <div class="controls">
                <a href="javascript:;" class="btn btn-small btn-info duplicator"><i class="icon-plus icon-white"></i> <?php print __('Aggiungi appuntamento'); ?></a>
            </div>
        </div>
    <div class="form-actions">
        <button type="submit" class="btn btn-large btn-primary"><i class="icon-ok icon-white"></i> <?php print __('Salva'); ?></button>
        <a href="<?php print URL::site('tour'); ?>" class="btn btn-large"><i class="icon-remove"></i> <?php print __('Annulla'); ?></a>
    </div>
</fieldset>
</form>