<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Recipientgroup extends Controller_Private {
    
    function action_index() {
        
        $this->template->title = __('Gruppi destinatari');
        $this->template->page_template = 'table';
        $this->template->content_template = 'recipientgroup_table';
        
        $this->template->actions = array(
            array('url' => URL::site('recipientgroup/add'), 'class' => 'btn-primary', 'icon_class' => 'icon-plus icon-white', 'label' => 'Aggiungi nuovo')
        );
        
        $rg = ORM::factory('recipientgroup')->filter_by_active_user();
        
        $pagination = Pagination::factory(array(
                'total_items' => $rg->count_all()
            )
        );
        
        $rg = $rg->add_recipients_details()->offset($pagination->offset)->limit($pagination->items_per_page)->find_all();
        
        $this->template->pagination = $pagination;
        $this->template->recipientgroups = $rg;
    }
    
    function action_add() {
        if ($this->request->method() == HTTP_Request::POST) {
            $rg = ORM::factory('recipientgroup')->values($_POST, array('name'));
            $rg->user_id = $this->active_user->id;
            try {
                $rg->save();
                Flashmessage::set('success', __('Modifiche effettuate con successo'));
                $this->request->redirect('recipientgroup/edit/'.$rg->id);
            } catch(ORM_Validation_Exception $e) {
                $errors = array_values($e->errors('models'));
                $this->template->messages['error'] = 
                        array_merge((array)$this->template->messages['error'], $errors);
            }
            
        }
        
        $this->template->form_values = $_POST;
        
        $this->template->title = __('Aggiungi gruppo destinatari');
        $this->template->page_template = 'form';
        $this->template->content_template = 'recipientgroup_form';
    }
    
    function action_edit() {
        
        $id = $this->request->param('id');
        
        if ($this->request->method() == HTTP_Request::POST) {
            $rg = ORM::factory('recipientgroup', $id)->values($_POST, array('name'));
            $rg->user_id = $this->active_user->id;
            try {
                $rg->save();
                $this->template->messages['success'][] = __('Modifiche effettuate con successo');
                
            } catch(ORM_Validation_Exception $e) {
                $errors = array_values($e->errors('models'));
                $this->template->messages['error'] = 
                        array_merge((array)$this->template->messages['error'], $errors);
            }
            
        }
        
        $rg = ORM::factory('recipientgroup', $id);
        if (!$rg->loaded()) {
            $this->request->redirect('recipientgroup');
        }
        
        $this->template->form_values = array();
        $this->template->form_values = array_merge($rg->as_array(), isset($_POST) ? $_POST : array());
        
        $this->template->title = __('Modifica gruppo destinatari');
        $this->template->page_subtitle = $rg->name;
        $this->template->page_template = 'form';
        $this->template->content_template = 'recipientgroup_form';
    }
    
    function action_delete() {
        $id = $this->request->param('id');
        $t = ORM::factory('recipientgroup', $id);
        if ($t->loaded()) {
            $t->delete();
            Flashmessage::set('success', __('Gruppo destinatari cancellato con successo'));
        }
        
        $this->request->redirect('recipientgroup');
    }
    
    function action_recipients() {
        $grp_id = $this->request->param('id');
        
        $group = ORM::factory('recipientgroup', $grp_id);
        
        $users = ORM::factory('recipient')
                ->where('recipientgroup_id', '=', $grp_id);
        $pagination = Pagination::factory(array(
                'total_items' => $users->count_all()
            )
        );
        $this->template->pagination = $pagination;
        
        $users = ORM::factory('recipient')
                ->where('recipientgroup_id', '=', $grp_id)
                ->offset($pagination->offset)
                ->limit($pagination->items_per_page)
                ->find_all();

        $out = array();
        $all_params = array();
        foreach($users as $user) {
            $params = $user->recipientparams->find_all();
            $out[$user->email]['email'] = $user->email;
            $out[$user->email]['id'] = $user->id;
            foreach($params as $param) {
                $all_params[$param->key] = true;
                $out[$user->email][$param->key] = $param->value;
            }
        }
        
        $this->template->actions = array(
            array('url' => URL::site('recipientgroup/import_recipients/'.$grp_id),
                'class' => 'btn-primary',
                'icon_class' => 'icon-plus icon-white',
                'label' => __('Aggiungi destinatari')),
            array('url' => URL::site('recipientgroup/'.$grp_id),
                'class' => '',
                'icon_class' => 'icon-chevron-left',
                'label' => __('Torna ai gruppi'))
        );
        
        $this->template->all_params = array_keys($all_params);
        $this->template->recipients = $out;
        $this->template->title = __('Destinatari');
        $this->template->page_subtitle = $group->name;
        $this->template->page_template = 'table';
        $this->template->content_template = 'recipient_table';
    }
    
    function action_import_recipients() {
        $grp_id = $this->request->param('id');
        $group = ORM::factory('recipientgroup', $grp_id);
        
        $this->template->actions = array(
            array('url' => URL::site('recipientgroup/'.$grp_id),
                'class' => '',
                'icon_class' => 'icon-chevron-left',
                'label' => __('Torna ai gruppi'))
        );
        
        $this->template->grp_id = $grp_id;
        $this->template->title = __('Aggiungi destinatari');
        $this->template->page_subtitle = $group->name;
        $this->template->page_template = 'form';
        $this->template->content_template = 'recipient_import_form';
        
    }
    
    function action_import_csv() {
        $grp_id = $this->request->param('id');
        $file = Validation::factory( $_FILES );
        $file->rule( 'file', array( 'Upload', 'not_empty' ) )
             ->rule( 'file', 'Upload::type', array(':value', array('csv')));
        
        if ($this->request->method() == HTTP_Request::POST && $file->check()) {
            $fname = Upload::save($file['file']);
            
            $csv = new parseCSV();
            $csv->auto($fname);
            $import_count = count($csv->data);
            
            if ($import_count > 0) {
                
                $info_msg = __('Il file contiene :num_contacts contatti. ', array(':num_contacts' => $import_count));
                if ($import_count > 10) {
                    $info_msg .= __('Il sistema mostra unicamente i primi 10 risultati. ');
                }
                $info_msg .= __('Scegli quali colonne importare e in quale colonna si trova l\'indirizzo email cliccando sui pulsanti presenti in ogni testata.');
                
                $this->template->messages['info'][] = $info_msg;
                
                $this->template->grp_id = $grp_id;
                $this->template->title = __('Revisione dati');
                $this->template->page_template = 'table';
                $this->template->content_template = 'recipient_import_csv';
                $this->template->import_data = $csv->data;
                $this->template->import_count = $import_count;
                $this->template->csv_fname = $fname;
                $this->template->form_action = URL::site('recipientgroup/elaborate_csv');
                
            } else {
                Flashmessage::set('error', __('Il file non contiene dati'));
                $this->request->redirect('recipientgroup/import_recipients/'.$grp_id);
            }
            
            
        } else {
            $errors = $file->errors('file_validation');
            Flashmessage::set('error', $errors['file']);
            $this->request->redirect('recipientgroup/import_recipients/'.$grp_id);
        }
    }
    
    function action_elaborate_csv() {
        
        // TODO migliorare logica update dei duplicati
        
        if ($this->request->method() == HTTP_Request::POST) {
            $grp_id = $this->request->post('id');
            $fname = $this->request->post('fname');
            $columns = $this->request->post('csv-field');
            $csv = new parseCSV();
            $csv->auto($fname);
            
            $key = array_search('envelope', $columns);
            $new_contacts = 0;
            $errors = 0;
            
            foreach($csv->data as $element) {
                $email_address = $element[$csv->titles[$key]];
                if (Valid::email($email_address)) {
                    
                    $recipient = ORM::factory('recipient');
                    $recipient->recipientgroup_id = $grp_id;
                    $recipient->email = $email_address;
                    try {
                        $recipient->save();
                        $new_contacts++;

                        $recipient_id = $recipient->id;
                        
                    } catch (Database_Exception $e) {
                        if ($e->getCode() == 1062) {
                            $recipient = ORM::factory('recipient')
                                    ->where('email', '=', $email_address)
                                    ->where('recipientgroup_id', '=', $grp_id)
                                    ->find();
                            $recipient_id = $recipient->id;
                        }
                        $errors++;
                    }

                    $i = 0;
                    foreach ($element as $k => $v) {
                        if ($columns[$i] == 'ok') {
                            $rp = ORM::factory('recipientparam');
                            $rp->recipient_id = $recipient_id;
                            $rp->key = Slug::ify($k, '_');
                            $rp->value = $v;
                            $rp->type = (is_numeric($v)) ? 'integer' : 'string'; // TODO servono altri tipi?
                            $rp->save();
                        }
                        $i++;
                    }
                }
            }
            
            // TODO migliorare messaggi.
            Flashmessage::set('success', __(':num_contatti importati con successo', array(':num_contatti' => $new_contacts)));
            Flashmessage::set('error', __(':num_errori errori', array(':num_errori' => $errors)));
            
            $this->request->redirect('recipientgroup/recipients/'.$grp_id);
        }
    }
    
    function action_recipient_edit() {
        
        if ($this->request->method() == HTTP_Request::POST) {
            // TODO save
            
        }
        
        $id = $this->request->param('id');
        $recipient = ORM::factory('recipient', $id);
        $grp_id = $recipient->recipientgroup_id;
        $recipientparams = ORM::factory('recipientparam')->where('recipient_id', '=', $id)->find_all();
        
        $this->template->title = __('Modifica destinatario');
        $this->template->page_subtitle = $recipient->email;
        $this->template->page_template = 'form';
        $this->template->content_template = 'recipient_form';
        $this->template->recipient = $recipient;
        $this->template->recipientparams = $recipientparams;       
    }
    
    function action_recipient_delete() {
        $id = $this->request->param('id');
        $recipient = ORM::factory('recipient', $id);
        $grp_id = $recipient->recipientgroup_id;
        $recipient->delete();
        Flashmessage::set('success', __('Contatto cancellato con successo'));
        $this->request->redirect('recipientgroup/recipients/'.$grp_id);
    }
    
    
}