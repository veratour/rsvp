<?php

class Model_Recipient extends ORM {
    
    protected $_belongs_to = array(
        'recipientgroups' => array()
    );
    
    protected $_has_many = array(
        'recipientparams' => array()
    );
    
    
    public function rules() {
        return array(
            'email' => array(
                array('not_empty'),
                array('email')
            )
        );
    }
}