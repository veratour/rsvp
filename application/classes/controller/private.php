<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Private extends Controller_Public {
    
    var $active_user;
    
    public function before() {
        parent::before();

        if (Auth::instance()->logged_in()) {
            $this->active_user = Auth::instance()->get_user();
        } else {
            $this->request->redirect(
                Route::get('auth')->uri(
                        array(
                            'action' => 'login',
                            'from'=> urlencode(Request::current()->url(TRUE))
                        )
                    ) . 
                    URL::query(array('from' => (Request::current()->url(TRUE))))
                );
        }
        
        if ($this->auto_render) {
            
            // TODO: mettere nel DB? In un file config?
            $this->template->nav = array (
                array( 'url' => URL::site('tour'), 'label' => __('Eventi')),
                array( 'url' => URL::site('recipientgroup'), 'label' => __('Destinatari')),
                array( 'url' => URL::site('form'), 'label' => __('Form')),
                array( 'url' => URL::site('messages'), 'label' => __('Messaggi')),
                array( 'url' => URL::site('landingpages'), 'label' => __('Pagine di atterraggio')),
            );
        }
        
    }
    
}