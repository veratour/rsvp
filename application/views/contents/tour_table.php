<table class="table table-striped table-bordered table-condensed">
<thead>
    <tr>
        <th width="30%"><?php print __('Nome'); ?></th>
        <th width="15%"><?php print __('Evento aperto'); ?></th>
        <th width="15%"><?php print __('# appuntamenti'); ?></th>
        <th width="10%"><?php print __('Dal'); ?></th>
        <th width="10%"><?php print __('Al'); ?></th>
        <th width="20%"><?php print __('Azioni'); ?></th>
    </tr>
</thead>
<tbody>
    <?php foreach($tours as $tour): ?>
    <tr>
        <td><?php print $tour->name ?></td>
        <td><?php print HTML::iconize_yes_no($tour->is_open); ?></td>
        <td><?php print $tour->events_count; ?></td>
        <td><?php print $tour->date_from; ?></td>
        <td><?php print $tour->date_to; ?></td>
        <td><?php print HTML::btn_modify(URL::site('tour/edit/'.$tour->id)).' '.HTML::btn_delete(URL::site('tour/delete/'.$tour->id)); ?></td>
    </tr>
    <?php endforeach; ?>
</tbody>
</table>