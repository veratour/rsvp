<?php

class Model_Selectionparam extends ORM {
    
    protected $_belongs_to = array(
        'selection' => array()
    );
    
    
    public function rules() {
        return array(
            'key' => array(
                array('not_empty')
            ),
            'operator' => array(
                array('not_empty')
            ),
            'value' => array(
                array('not_empty')
            )
        );
    }
}