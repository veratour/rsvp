<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php if ($title != '') print $title . ' :: '; ?>RSVP</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="RSVP">
    <meta name="author" content="Alessandro Balasco">
    
    <?php foreach ($styles as $style): ?>
        <?php print HTML::style($style['file'], $style['attributes'], $style['protocol'], $style['index']); ?>
    <?php endforeach; ?>
    
    <!--[if lt IE 9]>
      <?= HTML::script('http://html5shim.googlecode.com/svn/trunk/html5.js') ?>
    <![endif]-->
    
    
    <script language="javascript">
        var base_url = '<?php print URL::base(); ?>';    
    </script>
    
    <?php /* TODO!
    <!-- Le fav and touch icons
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
     -->
     */ ?>
  </head>

  <body class="<?php print Request::current()->controller() .'-'. Request::current()->action(); ?>">

    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="<?php print URL::site(); ?>"><img src="<?php print URL::site('assets/img/rsvp-logo.png'); ?>" width="75" height="24" alt="RSVP" /></a>
          <div class="nav-collapse">
            <?php if (isset($nav) && count($nav) > 0): ?>
            <ul class="nav">
              <?php foreach($nav as $n): ?>
              <li><a href="<?php print $n['url']; ?>"><?php print $n['label']; ?></a></li>
              <?php endforeach; ?>
            </ul>
            <?php endif; ?>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container">
        
        <?php print View::factory('pages/'.$page_template, compact(array_keys(get_defined_vars()))); ?>
        
        <footer class="footer">
            <p class="pull-right">
                <?php if (Auth::instance()->logged_in('active')): ?>
                <a href="<?php print URL::site('logout'); ?>"><?php print __('Logout'); ?></a> |
                <?php endif; ?>
                <a href="#"><?php print __('Torna in cima'); ?></a>
            </p>
            <p>&copy; <?php print date('Y'); ?> Alessandro Balasco</p>
        </footer>
        
    </div> <!-- /container -->

    <?php foreach ($scripts as $script): ?>
        <?php print HTML::script($script['file'], $script['attributes'], $script['protocol'], $script['index']); ?>
    <?php endforeach; ?>

    
    <?php //echo View::factory('profiler/stats') ?>
    
  </body>
</html>
