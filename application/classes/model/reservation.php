<?php

class Model_Reservation extends ORM {
    
    protected $_belongs_to = array(
        'message' => array()
    );
    
    
    public function rules() {
        return array(
            'reserved_at' => array(
                array('not_empty'),
                array('date')
            )
        );
    }
}