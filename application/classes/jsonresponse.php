<?php

class JsonResponse {
    
    public $success;
    public $message;
    
    function __construct($success = TRUE, $message = '') {
        $this->success = $success;
        $this->message = $message;
    }
    
    public function __toString() {
        return json_encode($this);
    }
}