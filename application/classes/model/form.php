<?php

class Model_Form extends ORM {
    
    protected $_has_many = array(
        'fields' => array(
            'model' => 'field',
            'through' => 'fields_forms'
        )
    );
    
    protected $_belongs_to = array(
        'users' => array()
    );
    
    public function rules() {
        return array(
            'name' => array(
                array('not_empty')
            )
        );
    }
    
    public function filter_by_active_user() {
        return $this->where('user_id', '=', Auth::instance()->get_user()->id);
    }
}