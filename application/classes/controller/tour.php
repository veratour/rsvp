<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Tour extends Controller_Private {
    
    function action_index() {
        
        $this->template->title = __('Eventi');
        $this->template->page_template = 'table';
        $this->template->content_template = 'tour_table';
        
        $this->template->actions = array(
            array('url' => URL::site('tour/add'), 'class' => 'btn-primary', 'icon_class' => 'icon-plus icon-white', 'label' => 'Aggiungi nuovo')
        );
        
        $tours = ORM::factory('tour')->filter_by_active_user();
        $pagination = Pagination::factory(array(
                'total_items' => $tours->count_all()
            )
        );
        
        $tours = $tours->add_events_details()->offset($pagination->offset)->limit($pagination->items_per_page)->find_all();
        $this->template->pagination = $pagination;
        $this->template->tours = $tours;
        
    }
    
    function action_add() {
        
        if ($this->request->method() == HTTP_Request::POST) {
            $tour = ORM::factory('tour')->values($_POST['tour'], array('name', 'is_open'));
            $tour->user_id = $this->active_user->id;
            try {
                $tour->save();
                foreach($_POST['event'] as $post_event) {
                    $event = ORM::factory('event')->values($post_event,
                            array('name', 'date_from', 'time_from',
                                  'date_to', 'time_to', 'location', 
                                  'description', 'city', 'address', 'country'));
                    
                    $event->tour_id = $tour->id;
                    $event->save();
                }
                Flashmessage::set('success', __('Evento aggiunto con successo'));
                $this->request->redirect('tour/edit/'.$tour->id);
            } catch(ORM_Validation_Exception $e) {
                $errors = array_values($e->errors('models'));
                // se il tour è stato salvato, vado direttamente ad edit, altrimenti rimango su add
                if ($tour->id) {
                    foreach($errors as $error) {
                        Flashmessage::set('error', $error);
                    }
                    $this->request->redirect('tour/edit/'.$tour->id);
                } else {
                    $this->template->messages['error'] = 
                        array_merge((array)$this->template->messages['error'], $errors);
                }
            }
        }
        
        $this->template->form_values = $_POST;
        
        $this->template->title = __('Aggiungi evento');
        $this->template->page_template = 'form';
        $this->template->content_template = 'event_form';
    }
    
    function action_edit() {
        
        $id = $this->request->param('id');

        if ($this->request->method() == HTTP_Request::POST) {
            $tour = ORM::factory('tour', $id)->values($_POST['tour'], array('name', 'is_open'));
            try {
                $tour->save();
                foreach($_POST['event'] as $post_event) {
                    if ($post_event['id']) {
                        $event = ORM::factory('event', $post_event['id']);
                    } else {
                        $event = ORM::factory('event');
                    }
                    $event->values($post_event,
                            array('name', 'date_from', 'time_from',
                                  'date_to', 'time_to', 'location', 
                                  'description', 'city', 'address', 'country'));
                    
                    $event->tour_id = $tour->id;
                    $event->save();
                }
                $this->template->messages['success'][] = __('Modifiche effettuate con successo');
            } catch (ORM_Validation_Exception $e) {
                $errors = array_values($e->errors('models'));
                $this->template->messages['error'] = 
                        array_merge((array)$this->template->messages['error'], $errors);
            }
        }
        
        $tour   = ORM::factory('tour', $id);
        if (!$tour->loaded()) {
            $this->request->redirect('tour');
        }
        $events = $tour->events->find_all();
        
        
        $this->template->form_values = array();
        $this->template->form_values['tour'] = array_merge($tour->as_array(), isset($_POST['tour']) ? $_POST['tour'] : array());
        
        if (isset($_POST['event'])) {
            $ce = max(count($events), count($_POST['event']));
        } else {
            $ce = count($events);
        }
        for ($i = 0; $i < $ce; $i++) {
            if (isset($events[$i])) {
                $this->template->form_values['event'][] = array_merge($events[$i]->as_array(), isset($_POST['event'][$i]) ? $_POST['event'][$i] : array());
            } else {
                $this->template->form_values['event'][] = isset($_POST['event'][$i]) ? $_POST['event'][$i] : array();
            }
        }
        
        $this->template->title = __('Modifica evento');
        $this->template->page_subtitle = $tour->name;
        $this->template->page_template = 'form';
        $this->template->content_template = 'event_form';
    }

    function action_delete() {
        $id = $this->request->param('id');
        $t = ORM::factory('tour', $id);
        if ($t->loaded()) {
            $t->delete();
            Flashmessage::set('success', __('Tour cancellato con successo'));
        }
        
        $this->request->redirect('tour');
    }
    
    // AJAX
    function action_delete_event() {
        $ok = FALSE;
        $id = $this->request->post('id');
        $e = ORM::factory('event', $id);
        if ($e->loaded()) {
            if ($e->delete()) $ok = TRUE;
        }
        if ($ok) {
            $this->response->body((string) new JsonResponse(TRUE, __('Evento eliminato con successo')));
        } else {
            $this->response->body((string) new JsonResponse(FALSE, __('Errore nell\'eliminazione dell\'evento')));
        }
        return;
    }
    
}