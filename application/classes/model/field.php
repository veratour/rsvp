<?php

class Model_Field extends ORM {
    
    protected $_belongs_to = array(
        'users' => array()
    );
    
    protected $_has_many = array(
        'forms' => array(
            'model' => 'form',
            'through' => 'fields_forms'
        )
    );
    
    public function rules() {
        return array(
            'key' => array(
                array('not_empty')
            ),
            'label' => array(
                array('not_empty')
            ),
            'type' => array(
                array('not_empty')
            )
        );
    }
    
    public function filter_by_active_user() {
        return $this->where('user_id', '=', Auth::instance()->get_user()->id);
    }
}