<?php if (isset($actions) && count($actions) > 0): ?>
<div class="actionbar btn-group">
    <?php foreach($actions as $action): ?>
    <a href="<?php print $action['url']; ?>" class="btn <?php print $action['class']; ?>"<?php if(isset($action['attributes'])) print ' '.$action['attributes']; ?>><i class="<?php print $action['icon_class']; ?>"></i> <?php print $action['label']; ?></a>
    <?php endforeach; ?>
</div>
<?php endif; ?>