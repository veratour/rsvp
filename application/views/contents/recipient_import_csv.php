<table class="table table-striped table-bordered table-condensed" id="import-csv-table">
<thead>
    <tr>
        <?php
        
        $head = $import_data[0];
        foreach($head as $k => $v) {
            ?><th class="state-ok"><button class="import-toggler btn btn-mini" data-state="ok"><i class="icon-ok"></i></button> <?php print $k; ?></th><?php
        }
        
        ?>
    </tr>
</thead>
<tbody>
    <?php for($i = 0; $i < min(10, $import_count); $i++): ?>
    <tr>
        <?php foreach ($import_data[$i] as $k => $v): ?>
        <td><?php print $v; ?></td>
        <?php endforeach; ?>
    </tr>
    <?php endfor; ?>
</tbody>
</table>

<form class="form-horizontal" method="post"<?php if(isset($form_action)) print ' action="'.$form_action.'"'; ?> id="import-csv-form">
    <input type="hidden" name="id" value="<?php print $grp_id; ?>" />
    <input type="hidden" name="fname" value="<?php print $csv_fname; ?>" />
<fieldset>
    <div class="form-actions">
        <button type="submit" class="btn btn-large btn-primary"><i class="icon-ok icon-white"></i> <?php print __('Conferma e salva'); ?></button>
        <a href="<?php print URL::site('recipientgroup'); ?>" class="btn btn-large"><i class="icon-remove"></i> <?php print __('Annulla'); ?></a>
    </div>
</fieldset>
</form>