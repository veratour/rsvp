<?php
$subtitle = '';
if (isset($page_subtitle)) $subtitle = ' <small>'.$page_subtitle.'</small>';
if (isset($page_title) && $page_title != '') print '<h1>'.$page_title.$subtitle.'</h1>';
else if ($title != '') print '<h1>'.$title.$subtitle.'</h1>';
?>