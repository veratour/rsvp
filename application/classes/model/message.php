<?php

class Model_Message extends ORM {
    
    protected $_belongs_to = array(
        'tours' => array(),
        'events' => array(),
        'recipientgroups' => array(),
        'selections' => array()
    );
    
    public function rules() {
        return array(
            'subject' => array(
                array('not_empty')
            ),
            'body' => array(
                array('not_empty')
            ),
            'scheduled_at' => array(
                array('date')
            )
        );
    }
}