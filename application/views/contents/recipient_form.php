<form class="form-horizontal" method="post"<?php if(isset($form_action)) print ' action="'.$form_action.'"'; ?>>
    <input type="hidden" name="id" value="<?php print $recipient->id; ?>" />
    <input type="hidden" name="recipientgroup_id" value="<?php print $recipient->recipientgroup_id; ?>" />
<fieldset>
    <legend>Destinatario</legend>
    <div class="control-group">
        <label class="control-label" for="email"><?php print __('Email'); ?></label>
        <div class="controls"><input type="text" name="email" id="email" class="input-xlarge" value="<?php print $recipient->email; ?>" /></div>
    </div>
    <?php foreach($recipientparams as $p): ?>
    <div class="control-group">
        <label class="control-label" for="<?php print $p->key; ?>"><?php print ucwords(Inflector::humanize($p->key)); ?></label>
        <div class="controls"><input type="text" name="recipientparam[<?php print $p->id; ?>]" id="recipientparam_<?php print $p->id; ?>" class="input-xlarge" value="<?php print $p->value; ?>" /></div>
    </div>
    <?php endforeach; ?>
    <div class="form-actions">
        <button type="submit" class="btn btn-large btn-primary"><i class="icon-ok icon-white"></i> <?php print __('Salva'); ?></button>
        <a href="<?php print URL::site('recipientgroup'); ?>" class="btn btn-large"><i class="icon-remove"></i> <?php print __('Annulla'); ?></a>
    </div>
</fieldset>
</form>