<?php

class Model_Event extends ORM {
    
    protected $_belongs_to = array(
        'tours' => array()
    );
    protected $_has_many = array(
        'landingpages' => array(),
        'messages' => array()
    );
    
    public function rules() {
        return array(
            'name' => array(
                array('not_empty')
            ),
            'date_from' => array(
                array('not_empty'), 
                array('date')
            ),
            'time_from' => array(
                array('time')
            ),
            'date_to' => array(
                array('date')
            ),
            'time_to' => array(
                array('time')
            ),
            'location' => array(
                array('not_empty')
            )
        );
    }
    
    public function filters() {
        return array(
            'time_from' => array(
                array(array($this, 'null_if_empty')),
            ),
            'date_to' => array(
                array(array($this, 'null_if_empty')),
            ),
            'time_to' => array(
                array(array($this, 'null_if_empty')),
            )
        );
    }
    
    public function null_if_empty($v) {
        return ($v == '') ? NULL : $v; 
    }
}