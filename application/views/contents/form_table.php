<table class="table table-striped table-bordered table-condensed">
<thead>
    <tr>
        <th><?php print __('Nome'); ?></th>
        <th><?php print __('Campi'); ?></th>
        <th width="20%"><?php print __('Azioni'); ?></th>
    </tr>
</thead>
<tbody>
    <?php foreach($forms as $f): ?>
    <tr>
        <td><?php print $f->name; ?></td>
        <td><?php $ff = array(); foreach($f->fields->find_all() as $formfield): $ff[] = $formfield->label; endforeach; print implode(', ', $ff); ?></td>
        
        <td><?php print HTML::btn_modify(URL::site('form/edit/'.$f->id)).' '.HTML::btn_delete(URL::site('form/edit/'.$f->id)); ?></td>
    </tr>
    <?php endforeach; ?>
</tbody>
</table>
