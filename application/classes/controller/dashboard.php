<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Dashboard extends Controller_Private {
    
    function action_index() {
        $this->template->title = __('Dashboard');
        $this->template->page_template = 'dashboard';
        $this->template->content_template = '';
    }
    
}