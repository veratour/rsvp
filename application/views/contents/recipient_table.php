<table class="table table-striped table-bordered table-condensed">
<thead>
    <tr>
        <th><?php print __('Email'); ?></th>
        <?php foreach($all_params as $param): ?>
        <th><?php print ucwords(Inflector::humanize($param)); ?></th>
        <?php endforeach; ?>
        <th width="20%"><?php print __('Azioni'); ?></th>
    </tr>
</thead>
<tbody>
    <?php foreach($recipients as $recipient): ?>
    <tr>
        <td><?php print $recipient['email'] ?></td>
        <?php foreach($all_params as $param): ?>
            <td><?php if (isset($recipient[$param])) print $recipient[$param]; ?></td>
        <?php endforeach; ?>
        <td><?php print HTML::btn_modify(URL::site('recipientgroup/recipient_edit/'.$recipient['id'])).' '.HTML::btn_delete(URL::site('recipientgroup/recipient_delete/'.$recipient['id'])); ?></td>
    </tr>
    <?php endforeach; ?>
</tbody>
</table>
