<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Public extends Controller_Template {
    
    var $template = 'layout';
    var $auto_render = true;
    
    public function before() {
        
        parent::before();
        
        if ($this->request->is_ajax() OR !$this->request->is_initial()) {
            $this->auto_render = FALSE;
        }    
        
        if ($this->auto_render) {
            $this->template->styles = array(
                array('file' => 'assets/twitter-bootstrap/less/bootstrap.css', 'attributes' => NULL, 'protocol' => NULL,'index' => NULL),
                array('file' => 'assets/css/style.css', 'attributes' => NULL, 'protocol' => NULL,'index' => NULL),
                array('file' => 'assets/css/datepicker.css', 'attributes' => NULL, 'protocol' => NULL,'index' => NULL),
                array('file' => 'assets/twitter-bootstrap/less/responsive.css', 'attributes' => NULL, 'protocol' => NULL,'index' => NULL),
            );
            $this->template->scripts = array(
                array('file' => 'assets/js/jquery-1.8.0.min.js', 'attributes' => NULL, 'protocol' => NULL,'index' => NULL),
                array('file' => 'assets/twitter-bootstrap/js.js', 'attributes' => NULL, 'protocol' => NULL,'index' => NULL),
                array('file' => 'assets/js/bootstrap-datepicker.js', 'attributes' => NULL, 'protocol' => NULL,'index' => NULL),
                array('file' => 'assets/js/rsvp.js', 'attributes' => NULL, 'protocol' => NULL,'index' => NULL),
            );
            $this->template->title = '';
            $this->template->page_title = '';
            $this->template->page_subtitle = '';
            $this->template->messages = Flashmessage::get();
            $this->template->actions = array();
            $this->template->nav = array();

        }
        
        if ($this->request->is_ajax()) {
            $this->response->headers('Content-type','application/json; charset='.Kohana::$charset);
        }
    }
    
    public function action_login() {
        $from = $this->request->query('from');
        $redirect = (isset($from)) ? $from : 'dashboard';
        
        if (Auth::instance()->logged_in('active')) $this->request->redirect($redirect);
        
        $this->template->title = __('Login');
        $this->template->page_template = 'miniform';
        $this->template->content_template = 'login';
        
        if ($this->request->method() == HTTP_Request::POST) {
            
            $remember = array_key_exists('remember', $this->request->post()) ? (bool) $this->request->post('remember') : FALSE;
            $user = Auth::instance()->login($this->request->post('username'), $this->request->post('password'), $remember);
            // If successful, redirect user
            if ($user) {
                if (Auth::instance()->logged_in('active')) {
                    $this->request->redirect($redirect);
                } else {
                    $this->template->messages['error'][] = __("Non hai i permessi per accedere al sistema.");
                }
            } else {
                $this->template->messages['error'][] = __("Errore di autenticazione: username o password errati.");
            }
        }
    }
    
    public function action_logout() {
        Auth::instance()->logout();
        $this->request->redirect(
                Route::get('auth')->uri(array(
                    'action' => 'login',
                )));
    }
    
    public function action_register() {
        // TODO register form
    }
    
    public function action_forgot_password() {
        // TODO forgot password
    }
}