<table class="table table-striped table-bordered table-condensed">
<thead>
    <tr>
        <th width="40%"><?php print __('Nome'); ?></th>
        <th width="20%"><?php print __('# partecipanti'); ?></th>
        <th width="20%"><?php print __('Azioni gruppo'); ?></th>
        <th width="20%"><?php print __('Azioni destinatari'); ?></th>
    </tr>
</thead>
<tbody>
    <?php foreach($recipientgroups as $rg): ?>
    <tr>
        <td><?php print $rg->name ?></td>
        <td><?php print $rg->recipient_count; ?></td>
        <td><?php
        
        print HTML::btn_modify(URL::site('recipientgroup/edit/'.$rg->id)).' ';
        print HTML::btn_delete(URL::site('recipientgroup/delete/'.$rg->id));
        
        ?></td>
        <td><?php
        
        print HTML::btn_list(URL::site('recipientgroup/recipients/'.$rg->id), 'Vedi', ($rg->recipient_count == 0)).' ';
        print HTML::btn_add(URL::site('recipientgroup/import_recipients/'.$rg->id));
        
        ?></td>
    </tr>
    <?php endforeach; ?>
</tbody>
</table>