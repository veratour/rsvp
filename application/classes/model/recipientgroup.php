<?php

class Model_Recipientgroup extends ORM {
    
    protected $_belongs_to = array(
        'users' => array()
    );
    
    
    public function rules() {
        return array(
            'name' => array(
                array('not_empty')
            )
        );
    }
    
    public function filter_by_active_user() {
        return $this->where('user_id', '=', Auth::instance()->get_user()->id);
    }
    
    public function add_recipients_details() {
        
        $subquery_count = DB::select('recipientgroup_id', array('COUNT("id")','recipient_count'))
                        ->from('recipients')
                        ->group_by('recipientgroup_id');
        
        return $this->select('*', 'a.*')
                    ->join(array($subquery_count, 'a'), 'LEFT')->on('recipientgroup.id','=','a.recipientgroup_id');
    }
}